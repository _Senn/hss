<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Tenali+Ramakrishna&display=swap" rel="stylesheet">


<title>🔥 WhatsApp Clone🔥</title>
<style type="text/css">
*{
margin:0;
padding:0;
outline:none;
-webkit-user-select:none;
box-sizing:border-box;

}
body{
background-color:white;
color:black;

font-family: 'Tenali Ramakrishna', sans-serif;
}

.icon1{
  width: 35px;
  height: 5px;
  background-color: white;
  margin: 6px 0;
}



/*. ============== SideNav ============. */
.sidenav {
  height:450px;
  width: 0;
  position: fixed;
  z-index: 1;
  top: 0;
  right:0;
  background-color: #111;
  overflow-x: hidden;
  transition: 0.3s;
  padding-top: 60px;
  }
  
  #open{
  float:right;
  color:white;
  cursor:pointer;
  margin-right:20px;
  
  }
  
  
  .sidenav a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
  }
  
  .sidenav a:hover {
  color: #f1f1f1;
  }
  .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
  }
  
  @media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
  }

/* ==============  Navbar. =========== */

#navbar{
background:#03841E;
padding-top:25px;
width:100%;
position:fixed;
top:0px;
color:white;
height:100px;
font-family: 'Tenali Ramakrishna', sans-serif;

}

#navbar #whatsapp{
margin-left:20px;
font-family: 'Tenali Ramakrishna', sans-serif;
margin-top:-3px;
font-size:37px;
font-weight:bold;

}



#navbar #search{
color:white;
font-size:27px;
float:right;
margin-right:25px;
margin-top:7px;

}


/* ==============  Chats. =========== */

#chats{
margin-top:120px;
margin-bottom:100px;
}

#chats #picture,#Name{
list-style:none;

}

#chats #picture{
margin-left:20px;
float:left;
/*
border:2px solid black;
*/

}
#chats #Name{
margin-left:75px;
/*
border:2px solid black;
*/

}

#chats #picture li{
width:65px;
height:65px;
/*
border:2px solid black;
*/
border-radius:50%;
margin-bottom:20px;

}

#chats #Name li{
height:65px;
/*
border:2px solid black;
*/
margin-left:25px;
margin-bottom:20px;
padding-bottom:0px;

}

#chats #Name li .all{
font-size:25px;
font-weight:bold;


}

#chats #picture li img{
width:65px;
height:65px;
border:2px solid black;
border-radius:50%;


}

#chats #Name .msg{
/*
border:2px solid black;
*/

margin-top:-6px;
font-family: 'Tenali Ramakrishna', sans-serif;
font-size:17px;
padding-bottom:10px;
border-bottom:1px solid black;
border-bottom-color: black;
}

#chats #Name .msg #fa{
color:blue;


}

h3{
font-size:20px;


}

#chats #Name .day1{
font-size:17px;
float:right;
margin-right:20px;


}





/* =============== footer. ============. */

#footer{
padding-top:10px;
text-align:center;
border-top:1px solid black;
padding-bottom:10px;
}

#footer #footer1{
font-size:17px;

}

</style>

</head>
<body id="body" >

<!-- ==============  Navbar. ===========  -->

<section id="navbar">
    <!--.   ========. SideNav ========.  -->
    <div id="mySidenav" class="sidenav">
    <a href="#" class="closebtn" onclick="closeNav()">&times;</a>
    <a href="#" onclick="darkmode()">Dark Mode On</a>
    <a href="#" onclick="lightmod()">Dark Mode Off</a>
    <a href="#">New group</a>
    <a href="#">New broadcast</a>
    <a href="#">WhatsApp web</a>
    <a href="#">Starred messages</a>
    <a href="#">Settings</a>
    </div>
    
    <span style="font-size:20px;cursor:pointer" class="icon"  id="open" onclick="openNav()">
    <div class="icon1" ></div>
    <div class="icon1" ></div>
    <div class="icon1" ></div>
    </span>
    <!--
    <span style="font-size:20px;cursor:pointer" class="icon"  id="open" onclick="openNav()">&#9776;</span>
    -->
    
    
    
    
    <!--
    <div class="search-box" id="search" >
    <input class="search-txt" value="search">
    <a href="#" class="search-btn">
    <i class="fa fa-search"></i>
    </a>
    </div>
    -->
    
    
    
    <i class="fa fa-search" id="search" class="icon" ></i>
  
    
    





    
    <!--
    <i class="fa fa-search" id="search" ></i>
    -->
    
    <!--
    </a>
    </div>
    -->
    
    <h2 id="whatsapp">WhatsApp</h2>
    <!--
    <div id="cam" ></div>
    <div class="all" id="nav1" >CHATS</div>
    <div class="all" id="nav2" >STATUS</div>
    <div class="all" id="nav3" >CALLS</div>
    -->
    

    
    
</section>


<!-- ==============  Chats. ===========  -->

<section id="chats" >
    <ul id="picture">
        <li><img src="https://api.sololearn.com/Uploads/Avatars/17339940.jpg"   ></li>
        <li><img src="https://api.sololearn.com/Uploads/Avatars/15609104.jpg"  ></li>
        <li><img src="https://api.sololearn.com/Uploads/Avatars/18602936.jpg" ></li>
        <li><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ6nAmAUJ4fn5b4XneBqMMB9l67Y9H7snMxITsX0a7aTw-d2Hby&usqp=CAU"   ></li>
        <li><img src="https://api.sololearn.com/Uploads/Avatars/4157142.jpg" ></li>
        <li><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTISeCzHIFBE4OBTeyz9KDOBOS9t0xEOP_UWCnrIMYph_D5dUgE&usqp=CAU"   ></li>
        <li><img src="https://api.sololearn.com/Uploads/Avatars/12155759.jpg"  ></li>
        <li><img src="https://api.sololearn.com/Uploads/Avatars/15622867.jpg"  ></li>
        <li><img src="https://api.sololearn.com/Uploads/Avatars/17716384.jpg"  ></li>
        <li><img src="https://api.sololearn.com/Uploads/Avatars/18552873.jpg"   ></li>
        <li><img src="https://api.sololearn.com/Uploads/Avatars/18388726.jpg"  ></li>
        <li><img src="https://api.sololearn.com/Uploads/Avatars/18588851.jpg"  ></li>
        <li><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR2mzqdAIIymhj-yz136O7I-VjaMV5o_nvOHjIs0faeS2XYZqhE&usqp=CAU"   ></li>        
        <li><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT4uQnNAG-fsWsALCx5w8sCwOnnlF64l7Vf6VeRDUGfnVHjlLBe&usqp=CAU"   ></li>        
        <li><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ48WCL2eTEqxYpRdpNK7B2_4kgahE2JdSRNj9br6iX5AKjG4Js&usqp=CAU"   ></li>        
            
        
    </ul>
    
    <ul id="Name">
        <li>
            <span id="day" class="day1"  >today</span>
            <span class="all" >🌀Dream Killer🗡️</span>
            <div class="msg" >
            <!--
                <span class="icon" >
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
                </span>
            -->
                <span class="msg2" >😎PUBG lover😎</span>
            </div>
        </li>
        
        
        <li>
            <span id="day" class="day1"  >today</span>
            <span class="all" >MASTER ASK</span>
            <div class="msg" >
            <!--
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
            -->
                <span>Hello, MASTER ASK</span>
            </div>
        </li>
        
        
        <li>
            <span id="day" class="day1"  >yesterday</span>
            <span class="all" >SHADOW KING</span>
            <div class="msg" >
            <!--
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
            -->
                <span>😂😂🤣😂</span>
            </div>
        </li>
        
        
        <li>
            <span id="day" class="day1"  >today</span>
            <span class="all" >My Love</span>
            <div class="msg" >
            <!--
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
            -->
                <span>Nice Drawing👍</span>
            </div>
        </li>
            
            
        <li>
            <span id="day" class="day1"  >today</span>
            <span class="all" >JEMA</span>
            <div class="msg" >
            <!--
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
            -->
                <span>You are a nice player👍</span>
            </div>
        </li>
        
        
        <li>
            <span id="day" class="day1"  >today</span>
            <span class="all" >Mummy</span>
            <div class="msg" >
            <!--
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
            -->
                <span>Happy Birthday mummy🎂🎂🍰</span>
            </div>
        </li>
        
        <li>
            <span id="day" class="day1"  >1/6/2020</span>
            <span class="all" >Farhanaz<span style="font-size:20px;" >🌟</span></span>
            <div class="msg" >
            <!--
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
            -->
                <span>You are a nice coder👍</span>
            </div>
        </li>
        
        
        <li>
            <span id="day" class="day1"  >today</span>
            <span class="all" >Dolly Chaudhary</span>
            <div class="msg" >
            <!--
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
            -->
                <span>Nice code 👌</span>
            </div>
        </li>
        
        <li>
            <span id="day" class="day1"  >yesterday</span>
            <span class="all" >Baby kim</span>
            <div class="msg" >
            <!--
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
            -->
                <span>Nice code 👌</span>
            </div>
        </li>
        
        
        <li>
            <span id="day" class="day1"  >today</span>
            <span class="all" >🌸Little Cherry🌸</span>
            <div class="msg" >
            <!--
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
            -->
                <span>Nice joke😂😂😂</span>
            </div>
        </li>
        
        <li>
            <span id="day" class="day1"  >1/6/2020</span>
            <span class="all" >Nikhat Shah</span>
            <div class="msg" >
            <!--
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
            -->
                <span>Nice code 👌</span>
            </div>
        </li>
        
        <li>
            <span id="day" class="day1"  >today</span>
            <span class="all" >💞SHAINA💞</span>
            <div class="msg" >
            <!--
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
            -->
                <span>Awesome code 👌</span>
            </div>
        </li>
        
        
        <li>
            <span id="day" class="day1"  >today</span>
            <span class="all" >Dady</span>
            <div class="msg" >
            <!--
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
            -->
                <span style="font-size:13px" ><i class="fa fa-camera"></i></span><span> Photo</span>
            </div>
        </li>
        
        
        <li>
            <span id="day" class="day1"  >yesterday</span>
            <span class="all" >Brother</span>
            <div class="msg" >
            <!--
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
            -->
                <span>Bye</span>
            </div>
        </li>
        
        
        <li>
            <span id="day" class="day1"  >today</span>
            <span class="all" >Sister</span>
            <div class="msg" >
            <!--
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
                <i class="fa fa-check" aria-hidden="true" id="fa"></i>
            -->
                <span>Hi didi</span>
            </div>
        </li>
        
        
    </ul>
</section>

<footer id="footer" >
    <div id="footer1" >&copy;copyright 2020-2023 | Made by Dream Killer</div>
    
</footer>    
    
<script>
function openNav() {
document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
document.getElementById("mySidenav").style.width = "0";
}

function darkmode(){
document.getElementById("body").style.backgroundColor = "#2f3332";
document.getElementById("body").style.color = "white";
document.getElementById("footer").style.borderTop = "1px solid white"
/*
document.getElementsByClassName("msg").style.borderBottom= "1px solid white";
*)



/*
if (document.getElementById("body").style.backgroundColor == "white"){
    document.getElementById("body").style.backgroundColor = "black";
    document.getElementById("body").style.color = "white";

}
else{
    document.getElementById("body").style.backgroundColor = "white";
    document.getElementById("body").style.color = "black";

}

/*
document.getElementsByClassName("msg").style.borderBottomColor = "white";
*/
}



function lightmod(){
document.getElementById("body").style.backgroundColor = "white";
document.getElementById("body").style.color = "black";
document.getElementById("footer").style.borderTop = "1px solid black"

}


</script>

</body>
</html>